define(['text!tweet/hot-list.html', "base/openapi", 'base/util', '../base/bigImg/bigImg', '../base/login/login', '../base/caretInsert/jquery.hammer.min'],
	function(viewTemplate, OpenAPI, Util, BigImg, Login) {
		return Piece.View.extend({
			id: 'tweet_hot-list',
			bigImg: null,
			events: {
				"click .tweetText": "goToHotDetail",
				"click .issueBtn": "goToIssueTweet",
				"click .atwho": "atwho",
				"click .tweetListImg": "goToUserInfor",
				"click .listImg": "bigImg"
			},
			listImgLoad: function(me) {
				console.info("----------")
				me.listIscroll.refresh();
			},
			deleteTweet: function(el) {
				var me = this;
				var dialog = new Piece.Dialog({
					autoshow: false,
					target: 'body',
					title: '确定删除此动弹？',
					content: ''
				}, {
					configs: [{
						title: '确认',
						eventName: 'ok'
					}, {
						title: '取消',
					}],
					ok: function() {
						//检查登陆
						var checkLogin = Util.checkLogin();
						if (checkLogin === false) {
							var login = new Login();
							login.show();
							return;
						}
						var that = me;
						var userToken = Piece.Store.loadObject("user_token");
						var accessToken = userToken.access_token;
						var $target = $(el.currentTarget);
						console.info($target);
						var id = $target.attr("data-id");
						// console.info(id);
						Util.Ajax(OpenAPI.tweet_delete, "GET", {
							access_token: accessToken,
							tweetid: id,
							dataType: 'jsonp'
						}, 'json', function(data, textStatus, jqXHR) {
							if (data.error === "200") {
								new Piece.Toast("删除成功");
								// that.onShow();
								Util.reloadPage('tweet/hot-list?reload=1')
							} else {
								new Piece.Toast(data.error_description);
							}
						}, null, null);
					}
				});

				dialog.show();
				$('.cube-dialog-screen').click(function() {
					dialog.hide();
				})

			},
			bigImg: function(el) {
				Util.bigImg(el);
			},
			goToUserInfor: function(imgEl) {
				//如果confirm出现，那么return，不进入用户界面。
				if (typeof $('.cube-dialog-screen').get(0) != 'undefined') {
					return;
				};
				console.info(imgEl);
				Util.imgGoToUserInfor(imgEl);
			},
			atwho: function(el) {
				var me = this;
				var $target = $(el.currentTarget);
				var id = $target.attr("data-ident");
				Util.Ajax(OpenAPI.user_information, "GET", {
					friend: id,
					dataType: 'jsonp'
				}, 'json', function(data, textStatus, jqXHR) {
					var author = data.name;
					author = encodeURI(author);
					var authorid = data.user;
					me.navigateModule("common/common-seeUser?" + "fromAuthor=" + author + "&fromAuthorId=" + authorid, {
						trigger: true
					});
				}, null);

			},
			goToHotDetail: function(el) {
				//如果confirm出现，那么return，不进入详情。
				if (typeof $('.cube-dialog-screen').get(0) != 'undefined') {
					return;
				};

				var $target = $(el.currentTarget);
				var id = $target.attr("data-id");

				//事件冒泡处理
				if (Util.eventBubble(el) == false) {
					return;
				}
				this.navigate("tweet-detail?id=" + id, {
					trigger: true
				});
			},
			goToIssueTweet: function() {
				var from = "hot-list";
				this.navigate("tweet-issue?from=" + from, {
					trigger: true
				});
			},
			render: function() {
				$(this.el).html(viewTemplate);
				Piece.View.prototype.render.call(this);
				return this;
			},
			listIscroll: null,
			onShow: function() {
				this.listIscroll = this.components['tweet-hot-list'].iScroll;

				bigImg = new BigImg();
				var reload = Util.request("reload") == 1 ? true : false;
				// tweet-hot-list
				Util.loadList(this, 'tweet-hot-list', OpenAPI.tween_hot_list, {
					'user': -1,
					'dataType': OpenAPI.dataType,
					'page': 1,
					'pageSize': 20
				}, reload);

				//绑定长按
				var me = this;
				me.longTouch(me);
				
				setTimeout(function() {
					me.listImgLoad(me);
				}, 2000)
				
				var windowHeight = $(window).height();

				var elTop = $("#tweet-hot-list").offset().top;
				var footerHeight = 50;
				var elHeight = windowHeight - elTop - 50;
				$("#tweet-hot-list").css({
					"height": elHeight
				});
				$("#tweet-hot-list").css("bottom", "0px");


			},
			longTouch: function(me) {
				var hammertime = $(me.el).find("#tweet-hot-list").hammer();
				// console.info(hammertime);
				// on elements in the toucharea, with a stopPropagation
				hammertime.on("hold", ".tweetList", function(ev) {
					me.deleteTweet(ev);
				});

			}
		}); //view define

	});