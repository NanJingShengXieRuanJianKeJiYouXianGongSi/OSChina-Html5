define(['text!news/blog-list.html', '../base/util', "../base/openapi"],
	function(viewTemplate, Util, OpenAPI) {
		return Piece.View.extend({
			id: 'news_blog-list',
			events: {
				"click .right-banner": "goNewsSearch",
				"click .newsList": "goToBlogDetail"
			},
			render: function() {
				$(this.el).html(viewTemplate);

				Piece.View.prototype.render.call(this);
				return this;
			},
			onShow: function() {
				Util.loadList(this, 'news-news-blog', OpenAPI.news_blog, {
					'catalog': 3,
					'dataType': OpenAPI.dataType,
					'page': 1,
					'pageSize': OpenAPI.pageSize
				});
			},

			goNewsSearch: function() {
				this.navigate("news-search", {
					trigger: true
				});
			},
			goToBlogDetail: function(el) {
				var $target = $(el.currentTarget);
				var id = $target.attr("data-id");
				var from = $('.active').attr('data-value').split('/')[1];
				var checkDetail= "news/news-blog-detail" ;
				var type = 3 ;
				//comment list 
				var comment = 5 ;
				this.navigate("news-blog-detail?id=" + id + "&from=" + from + "&checkDetail=" + checkDetail +"&fromType=" +type +"&com=" + comment, {
					trigger: true
				});
			},
		}); //view define

	});