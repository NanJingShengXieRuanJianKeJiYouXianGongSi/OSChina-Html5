define(['text!../common/comment-message.html', "../base/openapi", '../base/util', '../base/login/login'],
	function(viewTemplate, OpenAPI, Util, Login) {
		return Piece.View.extend({
			id: 'tweet_comment-message',
			login: null,
			events: {
				"click .backBtn": "goBack",
				"click .reply": "commentReply"
			},
			goBack: function() {
				history.back();
			},
			commentReply: function() {
				Util.checkLogin();
				var checkLogin = Util.checkLogin();
				if (checkLogin === false) {
					login.show();
				} else {
					this.commentReply1();
				}
			},
			commentReply1: function() {
				var replyCont = $(".replyTextarea").val();
				replyCont = replyCont.replace(/(\s*$)/g, ""); //去掉右边空格
				var isNotice = $(".replyNotice").is(":checked");;
				if (replyCont === "" || replyCont === " ") {
					new Piece.Toast('请输入评论内容');
				} else {
					var commentCont = $(".replyTextarea").val();
					var userToken = Piece.Store.loadObject("user_token");
					var accessToken = userToken.access_token;
					var tweetId = Util.request("userId");

					Util.Ajax(OpenAPI.comment_pub, "GET", {
						access_token: accessToken,
						id: tweetId,
						catalog: 4,
						content: commentCont,
						dataType: 'jsonp'
					}, 'json', function(data){
						console.info(data);
						if(data.error=="200") {
							new Piece.Toast("留言发表成功");
							setTimeout(function() {
								history.back();

							},500);
							
						}else {
							if(data.error_description !=="") {
								new Piece.Toast(data.error_description);
							}else {
								new Piece.Toast("请求出错，请稍后再试！");
							}
							
						}

						

					}, null, null);
				}

			},
			render: function() {
				login = new Login();
				$(this.el).html(viewTemplate);

				Piece.View.prototype.render.call(this);
				return this;
			},
			onShow: function() {
				var me = this ;
				var userName = Util.request("userOp");
				userName = decodeURI(decodeURI(userName));
				$("#mainTweet").html("");
				$("#mainTweet").html("发送留言给" + userName);
				$('.replyTextarea').focus();
				// 小屏幕出现小键盘后textarea布局改变问题
				// $(window).resize(function() {
				// 	me.showkeyboard();
					
				// });
				
			}
			// showkeyboard:function(){
			// 	var availHeight = window.screen.availHeight ;
			// 	if(availHeight<230){
			// 		$('.replyTextarea').css("height","65px")
			// 	}
				
			// }
		}); //view define

	});